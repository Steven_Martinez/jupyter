import sys
#from math import sin #para usar la función seno
#from time import time #importamos la función time para ca
import numpy as np
import h5py

#tiempo_inicial = time() 
#Xo
Xo = int(sys.argv[1])
#Yo
Yo = int(sys.argv[2])
#N
n = int(sys.argv[4])

#Declaracion de array
Xn = np.zeros(n+1)
Yn = np.zeros(n+1)
#Intervalos entre Xo - Xn
Xn = (np.arange(Xo, (int(sys.argv[3])*10)+1)/10)
#Asignamos el valor Yo a la primera posicion del vector Yn
Yn[0] = Yo

def get_h():
    return ((Xn[len(Xn)-1] - Xn[0]) / n)

h = get_h()

def get_Yn():
    for i in range(1, n+1):
        Yn[i] = (np.around((Yn[(i-1)] + h *(3*(Xn[i-1])-2*(Yn[i-1]))), 2))

get_Yn()

def llenar_matriz():
    aux = np.zeros((n+1,3))
    for i in range(n+1):
        aux[i:,0] = i
        aux[i:,1] = Xn[i]
        aux[i:,2] = Yn[i]
    return aux
#Creacion del archivo h5
hf = h5py.File('euler.h5', 'w')
hf.create_dataset('dataset_1', data=llenar_matriz())
hf.close()

#tiempo_final = time() 
 
#tiempo_ejecucion = tiempo_final - tiempo_inicial
 
#print ('El tiempo de ejecucion fue:',tiempo_ejecucion)